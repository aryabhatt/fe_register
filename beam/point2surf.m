function [d, u, v] = point2surf (pts)

% create model
[x, y] = ellipse ([0,0], 8, 5, 100);
srf = [x, y];

% size of point clouds
na = size(pts, 1);
sxy = [srf(end,:); srf; srf(1,:)];
nb = size(sxy, 1);

% find linear fits beteen points
m    =  (sxy(2:nb,2)-sxy(1:nb-1,2))./(sxy(2:nb,1)-sxy(1:nb-1,1));
tmp1  = sxy(1:nb-1,2) - m.*sxy(1:nb-1,1);
tmp2  = sqrt(1+m.^2);

% equation of line in the form : Ax + By + C = 0 :
A = m./tmp2;
B = -1./tmp2;
C = tmp1./tmp2;

% allocate memory
d = zeros (na, 1);
u = zeros (na, 1);
v = zeros (na, 1);

% distance between points and sxyace
for i = 1 : na
    minxy = zeros(3,2);
    dall = (pts(i,1)-sxy(2:nb-1,1)).^2 + (pts(i,2)-sxy(2:nb-1,2)).^2;
    [d1, j] = min (dall);
    j = j+1;
    minxy(1,:) = sxy(j,:);
    [d2, minxy(2,:)] = mindist (pts(i,:), sxy(j-1,:), sxy(j,:), A(j-1), B(j-1), C(j-1));
    [d3, minxy(3,:)] = mindist (pts(i,:), sxy(j,:), sxy(j+1,:), A(j), B(j), C(j));
    [d(i), j] = min([d1, d2, d3]);
    u(i) = minxy(j,1) - pts(i,1);
    v(i) = minxy(j,2) - pts(i,2);
end
end

function [d, pt_int] = mindist (pt0, pt1, pt2, A, B, C1)
    xcrd = [pt1(1), pt2(1)];
    ycrd = [pt1(2), pt2(2)];
    
    % find the point of intersection
    lhs = [A B; -B, A];   
    C2 = B*pt0(1) - A*pt0(2);
    rhs = [-C1; -C2];
    pt_int = (lhs')*rhs;
    
    % point of intersection
    x = pt_int(1);
    y = pt_int(2);
 
    % if (x,y) lines between pt1 and pt2 get the distance otherwise
    % return a large number
    xcrd = sort(xcrd);
    ycrd = sort(ycrd);
    if ((x > xcrd(1)) && (x < xcrd(2)) && (y > ycrd(1)) && (y < ycrd(2)))
        d =  abs(A*pt0(1) + B*pt0(2) + C1);
    else
        d = 1000;
    end
end