function FEARegister


% Initial Finite-Element mesh
%[x, y] = circle ([0,0], 5, 100);
pts = [0, 0; 10, 0; 10, 1; 0, 1];
hmax = 0.25;
tiny = hmax / 10;
hdata = struct('hmax', hmax);
options = struct('output', false);
[nodes, elems] = mesh2d (pts, [], hdata, options);
npts = size (nodes, 1);
% search for boundary nodes

%[~, ~, ~, ibnd] = connectivity (nodes, elems);
ebnd = false (npts, 1);
ebnd (nodes(:,1) < tiny) = true;
nbnd = false (npts, 1);
nbnd (nodes(:,1) > 10 - tiny) = true;

% run Finite-Element Analysis
u = fem (nodes, elems, ebnd, nbnd);
nodes = nodes + u;
patch('Faces', elems, 'Vertices', nodes, 'Facecolor', 'w');
hold on;
quiver(nodes(:,1), nodes(:,2), u(:,1), u(:,2));
hold off;
axis equal;
disp(u);
end