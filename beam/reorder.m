function edges = reorder (pts)

npts = size (pts, 1);
cen = mean (pts);

% shift the coords 
pts = bsxfun (@minus, pts, cen);
rad = sqrt(pts(:,1).^2 + pts(:,2).^2);
kdtree = createns (pts, 'NsMethod', 'kdtree', 'distance', 'minkowski');

% order points w.r.t neighborhood
edges = zeros (npts, 2);
flag = false (npts, 1);
edges(1,:) = pts(1,:);
flag(1) = true;
for i = 2 : npts
    [j, d] = knnsearch (kdtree, edges(i-1,:), 'k', 11);
    if (d(1) ~= 0)
        error('FUMTU!!');
    end
    % calc angels 
    sgn = sign (pts(j,2));
    ang = acos (pts(j,1)./rad(j));
    ang(sgn < 0) = 2*pi - ang(sgn < 0);
    if (any(ang < pi) && any(ang > pi))
        ang(sgn > 0) = ang(sgn > 0) + 2*pi;
    end

    % find the counter-clockwise next point
    k = find (ang > ang(1), 1);
    if (~isempty (k))
        ineigh = j(k);
    elseif (ang(1) - min(ang) > pi)
        [~, k] = min(ang);
        ineigh = j(k);
    else
        error('Some snafu here');
    end
    if (~flag(ineigh))
        edges(i,:) = pts(ineigh,:);
        flag(ineigh) = true;
    else
        error('Unable to find next edge');
    end
end
if (~any(flag))
    error('More fubar.');
end
edges = bsxfun(@plus, edges, cen);