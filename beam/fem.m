function u = fem (nodes, elems, ebnd, nbnd)
% huff

% assemble global matrix
npts = size(nodes, 1);
idom = ~ebnd;
ksys = assemble_linsys (elems, nodes);
ii = false(2*npts,1);
ii(1:2:end) = idom;
ii(2:2:end) = idom;

% calc forces
%fsys = calc_bndf (nodes, elems, nbnd);
fsys = assemble_rhs (nodes, elems);

% Essential BCs
u = zeros (npts, 2);

% solve the linear system
d = ksys (ii,ii) \ fsys (ii);
ndom = npts - length(find(ebnd));
u(idom,:) = reshape(d, 2, ndom)';
