function val = N (i, x1, x2)
switch (i)
    case 1
        val = 1 - x1 - x2;
    case 2
        val = x1;
    case 3
        val = x2;
end