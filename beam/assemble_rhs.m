function f = assemble_rhs (nodes, elems)

nelm = size (elems, 1);
npts = size (nodes, 1);
f = zeros (2*npts, 1);
weight = -9.8 * 7800;
jacobian_det = @(x, y) (x(2)-x(1))*(y(3)-y(1)) - (x(3)-x(1))*(y(2)-y(1));
for i  = 1 : nelm
    inode = elems(i,:);
    x = nodes(elems(i,:),1);
    y = nodes(elems(i,:),2);
    jac = jacobian_det (x, y);
    for j = 1:3
        f(2*inode(j)-1) = 0;
        f(2*inode(j)) = f(2*inode(j)) + weight * N(j, 0.333, 0.333) * jac;
    end
end