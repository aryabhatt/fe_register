function Ksys = assemble_linsys (elems, nodes)
%
% Assembles the linear system
% ELEMS defines the ordering of the nodes
% nodes provide the coordinates

npts = size(nodes, 1);
nelm = size(elems, 1);
Ksys = zeros (2*npts);
E = 2.0E+011;
nu = 0.3;

for k = 1 : nelm
    inode = elems(k,:);
    xcrd = nodes(inode, 1);
    ycrd = nodes(inode, 2);
    ke = isoparatri (E, nu, xcrd, ycrd);
    ndof = zeros (1,6);
    for i=1: 3
        ndof(2*i-1:2*i) = [2*inode(i)-1, 2*inode(i)];
    end
    Ksys(ndof,ndof) = Ksys(ndof,ndof) + ke;
end