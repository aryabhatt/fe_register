function f = calc_bndf (nodes, elems, nbnd)

nelm = size (elems, 1);
npts = size (nodes, 1);

% u = ||d||
force = zeros (npts,2);
force(nbnd,1) = 1.0E08;

f = zeros(2*npts, 1);
theJacobian = @(x, y) ([x(2)-x(1), y(2)-y(1); x(3)-x(1), y(3)-y(1)]);
for i  = 1 : nelm
    inode = find (nbnd (elems(i,:)));
    if (length (inode) == 2)
        x = [nodes(elems(i,1),1), nodes(elems(i,2),1), nodes(elems(i,3),1)];
        y = [nodes(elems(i,1),2), nodes(elems(i,2),2), nodes(elems(i,3),2)];
        jac = theJacobian (x ,y);
        fbnd = [0, 0; 0, 0];
        n1 = elems(i, inode(1));
        n2 = elems(i, inode(2));
        if (inode(1) == 1 && inode(2) == 2)
           ds = sqrt(jac(1,1)^2 + jac(1,2)^2);
           fq1 = 0.5 * (force(n1,:) + force(n2,:));
           fbnd(1,:) = fq1 * ds;
           fbnd(2,:) = fq1 * ds;
        elseif (inode(1) == 2 && inode(2) == 3)
            ds = sqrt((jac(2,1)-jac(1,1))^2 + (jac(2,2)-jac(1,2))^2);
            fq1 =  0.5 * (force(n1,:) + force(n2,:));
            fbnd(1,:) = fq1 * ds;
            fbnd(2,:) = fq1 * ds;
        elseif (inode(1) == 1 && inode(2) == 3)
            ds = sqrt(jac(2,1)^2 + jac(2,2)^2);
            fq1 = 0.5 *(force(n1,:) + force(n2,:));
            fbnd(1,:) = fq1 * ds;
            fbnd(2,:) = fq1 * ds;
        else
            error('Some bullshit happened.');
        end
        %arrow (nodes(n1,:), nodes(n2,:));
        %hold on; axis([-5, 5, -5, 5]); drawnow;
        f(2*n1-1) = f(2*n1-1) + fbnd(1,1);
        f(2*n1) = f(2*n1) + fbnd(1,2);
        f(2*n2-1) = f(2*n2-1) + fbnd(2,1);
        f(2*n2) = f(2*n2) + fbnd(2,2);
    end
end
