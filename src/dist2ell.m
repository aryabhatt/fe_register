
function [d, x, y] = dist2ell (major, minor, pt)
asq = major * major;
bsq = minor * minor;
u(1,1) = pt(1);
u(2,1) = sign (pt(2)) * real(sqrt(bsq * (1 - u(1)^2/asq)));
u(3,1) = 0;
f_pre = 0;
err = 1e10;
tol = 1e-06;
maxiter = 30;
iter = 0;
while ((err > tol) & (iter < maxiter))
    f = funct (u, pt, asq, bsq);
    err = norm(f - f_pre);
    J = jacob (u, asq, bsq);
    du = -J\f;
    u = u + du;
    f_pre = f;
    iter = iter + 1;
end
if (iter >= maxiter )
    warning('No of iterations exceeded the MaxIter value');
end
d = sqrt ((pt(1) - u(1))^2 + (pt(2) - u(2))^2);
if ( nargout > 1)
    x = u(1);
    y = u(2);
end
end

function f = funct (u, pt, asq, bsq)
f = zeros (3,1);
f(1) = u(1) - pt(1) + u(3) * u(1) / asq;
f(2) = u(2) - pt(2) + u(3) * u(2) / bsq;
f(3) = u(1) * u(1) /asq + u(2) * u(2) / bsq - 1;
end

function J = jacob (u, asq, bsq)
J = zeros (3);
J(1,:) = [1 + u(3)/asq, 0, u(1) / asq];
J(2,:) = [0, 1 + u(3)/bsq, u(2) / bsq];
J(3,:) = [2 * u(1) / asq, 2 * u(2) / bsq, 0];
end