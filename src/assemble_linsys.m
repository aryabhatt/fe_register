function Ksys = assemble_linsys (elems, nodes)
%
% Assembles the linear system
% ELEMS defines the ordering of the nodes
% nodes provide the coordinates

npts = size(nodes, 1);
nelm = size(elems, 1);
Ksys = zeros (2*npts);

% constants
% Young's modulus
E = 1E+08;
% Poisson's ratio
nu = 0.3;

for k = 1 : nelm
    inode = elems(k,:);
    % Ordering is important
    xcrd = [nodes(inode(1),1), nodes(inode(2),1), nodes(inode(3),1)];
    ycrd = [nodes(inode(1),2), nodes(inode(2),2), nodes(inode(3),2)];
    ke = isoparatri (E, nu, xcrd, ycrd);
    ndof = zeros(1,6);
    for i=1:3
        ndof(2*i-1:2*i) = [2*inode(i)-1, 2*inode(i)];
    end
    Ksys(ndof,ndof) = Ksys(ndof,ndof) + ke;
end