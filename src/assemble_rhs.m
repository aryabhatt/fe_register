function f = assemble_rhs (nodes, elems, ibnd, surf, pres, use_pres)

nelm = size (elems, 1);
npts = size (nodes, 1);
f = zeros (2*npts, 1);

alpha = 0.1;
beta = 50;
h = 1;

% Rotation matrix
M = [0, 1; -1, 0];

theJacobian = @(x, y) ([x(2)-x(1), y(2)-y(1); x(3)-x(1), y(3)-y(1)]);
for i  = 1 : nelm
    inode = find (ibnd (elems(i,:)));
    if (length (inode) == 2)
        x = [nodes(elems(i,1),1), nodes(elems(i,2),1), nodes(elems(i,3),1)];
        y = [nodes(elems(i,1),2), nodes(elems(i,2),2), nodes(elems(i,3),2)];
        jac = theJacobian (x ,y);
        fbnd = [0, 0; 0, 0];
        n1 = elems(i, inode(1));
        n2 = elems(i, inode(2));
        
        % find nearest points on fixed surface to n1 and n2
        [~, s1] = knnsearch (surf, nodes(n1,:), 'k', 1);
        [~, s2] = knnsearch (surf, nodes(n2,:), 'k', 1);
        f1 = pres * exp(-(s1/h)^2);
        f2 = pres * exp(-(s2/h)^2);
        
        if (inode(1) == 1 && inode(2) == 2)
            d = [x(2)-x(1); y(2)-y(1)];
            nrm = (M * d / norm(d, 2))';
            ds = sqrt(jac(1,1)^2 + jac(1,2)^2);
            
            % one-point quadrature
            qpt = 0.5 * (nodes(n1,:) + nodes(n2,:));
            if (use_pres)
                force = (alpha*pres - 0.5*(f1+f2));
            else
                [s, u, v] = ptforce (surf, qpt, nrm);
                amp = beta * log (1+abs(s));
                force = amp * dot (nrm, [u, v]);
            end
            fbnd(1,:) = 2 * force * ds * nrm;
            fbnd(2,:) = 2 * force * ds * nrm;
        elseif (inode(1) == 2 && inode(2) == 3)
            d = [x(3)-x(2); y(3)-y(2)];
            nrm = (M * d / norm(d, 2))';
            ds = sqrt((jac(2,1)-jac(1,1))^2 + (jac(2,2)-jac(1,2))^2);
            
            % one-point quadrature
            qpt = 0.5 * (nodes(n1,:) + nodes(n2,:));
            if (use_pres)
                force = (alpha*pres - 0.5*(f1+f2));
            else
                [s, u, v] = ptforce (surf, qpt, nrm);
                amp = beta * log (1 + abs(s));
                force = amp * dot (nrm, [u, v]);
            end
            fbnd(1,:) = 2 * force * ds * nrm;
            fbnd(2,:) = 2 * force * ds * nrm;
        elseif (inode(1) == 1 && inode(2) == 3)
            d = [x(1)-x(3); y(1)-y(3)];
            nrm = (M * d / norm(d, 2))';
            ds = sqrt(jac(2,1)^2 + jac(2,2)^2);
            
            % one-point quadrature
            qpt = 0.5 * (nodes(n1,:) + nodes(n2,:));
            if (use_pres)
                force = (alpha*pres - 0.5*(f1+f2));
            else
                [s, u, v] = ptforce (surf, qpt, nrm);
                amp = beta * log (1+abs(s));
                force = amp * dot (nrm, [u, v]);
            end
            fbnd(1,:) = 2 * force * ds * nrm;
            fbnd(2,:) = 2 * force * ds * nrm;
        else
            error('Couldn''t order the boundary nodes.');
        end
        f(2*n1-1) = f(2*n1-1) + fbnd(1,1);
        f(2*n1) = f(2*n1) + fbnd(1,2);
        f(2*n2-1) = f(2*n2-1) + fbnd(2,1);
        f(2*n2) = f(2*n2) + fbnd(2,2);
    end
end