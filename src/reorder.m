function edges = reorder (pts, pt2)

npts = size (pts, 1);
cen = mean (pts);

% shift the coords 
pts = bsxfun (@minus, pts, cen);
pt2 = pt2 - cen;
kdtree = createns (pts, 'NsMethod', 'kdtree', 'distance', 'minkowski');

% order points w.r.t neighborhood
edges = zeros (npts, 2);
flag = false (npts, 1);
edges(1,:) = pts(1,:);
edges(2,:) = pt2;
flag(1:2) = true;
n = 2;
while ( n < npts)
    p = 2;
    while (1)
        [j, ~] = knnsearch (kdtree, edges(n,:), 'k', p + 1);
        if isempty (find(~flag(j),1))
            p = 2 * p;
        else
            break
        end
    end
    for i = j
        if ~flag(i)
            edges(n+1,:) = pts(i,:);
            flag (i,:) = true;
            n = n + 1;
            break;
        end
    end
end
edges = bsxfun(@plus, edges, cen);