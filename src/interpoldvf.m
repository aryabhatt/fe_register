function [du, dv] = interpoldvf (nodes, elems, u, xctrl, yctrl)

nelm = size (elems, 1);
xcrd = xctrl(1,:);
ycrd = yctrl(:,1);
du = zeros (size (xctrl));
dv = zeros (size (yctrl));
dx = xcrd(2) - xcrd(1);
dy = ycrd(2) - ycrd(1);

for ielm = 1 : nelm
    inode = elems (ielm,:);
    x = nodes (inode, 1);
    y = nodes (inode, 2);
    [t, c] = transform (x, y);
    jbeg = floor((min(x) - xcrd(1))/dx)+1;
    jend = ceil ((max(x) - xcrd(1))/dx);
    ibeg = floor((min(y) - ycrd(1))/dy)+1;
    iend = ceil ((max(y) - ycrd(1))/dy);
    for j = jbeg : jend
        for i = ibeg : iend
            eta = t * ([xcrd(j); ycrd(i)] - c);
            if (all(eta >= 0 & eta <= 1) && sum(eta) <= 1)
                N = [1-sum(eta), eta(1), eta(2)];
                du(i,j) = dot (N, u(inode, 1));
                dv(i,j) = dot (N, u(inode, 2));
            end
        end
    end
end

function [t, c] = transform (x, y)
    t = inv([x(2)-x(1), x(3)-x(1); y(2)-y(1), y(3)-y(1)]);
    c = [x(1); y(1)];
end
end