function [obnd, ibnd] = search_innout (con, bnd)

% allocate memory
obnd = false(size(bnd));
ie = all(bnd(con),2);
eb = con(ie,:);
ne = size (eb,1);

% the first node is most likely to be the first
% point in the list input to mesh2d. We start from
% the point in edges and break when we return to it.
p0 = eb(1,1);
prev = p0;
next = eb(1,2);
obnd(1) = true;
for i = 2 : ne
    ii = find(eb(:,1)==next);
    jj = find(eb(:,2)==next);
    if ~isempty(ii)
        if (numel(ii)==1 && eb(ii,2) ~= prev)
            prev = next;
            next = eb(ii,2);
            obnd(prev) = true;
            continue;
        elseif numel(ii) == 2
            j = eb(ii,2)~=prev;
            prev = next;
            next = eb(ii(j),2);
            obnd(prev) = true;
            continue;
        end
    end
    if ~isempty (jj)
        if numel(jj) == 1 && eb(jj,1) ~= prev
            prev = next;
            next = eb(jj,1);
            obnd(prev) = true;
            continue;
        elseif numel(jj)==2
            j = eb(jj,1)~=prev;
            prev = next;
            next = eb(jj(j),1);
            obnd(prev) = true;
            continue;
        end
    end
    if next == p0
        break;
    end
end
ibnd = (bnd & ~obnd);