function inner = create_shell (pts)
cen  = mean(pts);
off = 2;
M = [0, -1; 1, 0];
xout = pts(:,1)-cen(1);
yout = pts(:,2)-cen(2);
N = size(pts,1);
inn = zeros(N,2);

for i = 1 : N
    if i < N
        dx = xout(i+1) - xout(i);
        dy = yout(i+1) - yout(i);
    else
        dx = xout(1) - xout(N);
        dy = yout(1) - yout(N);
    end
    d  = sqrt(dx^2 + dy^2);
    
    % inward normal 
    n = M * [dx/d; dy/d] * off;  
    inn(i,1) = xout(i) + n(1);
    inn(i,2) = yout(i) + n(2);
end
%plot(xout, yout, 'r', inn(:,1), inn(:,2),'k.');
inn = bsxfun(@plus, inn, cen);
% done manually
inner = inn(inn(:,1)>-44.15,:);
inner = smooth_curv (inner(:,1), inner(:,2), 20);
end