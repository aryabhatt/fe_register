function ifix = findfixednodes (nodes, ibnd)
%ifix = find ((nodes(:,2) < -30.0) & ibnd);
ii = (nodes(:,1) < -48.435) & ibnd;
jj = (nodes(:,1) > 54.5) & (nodes(:,2) > 1) & ibnd;
ifix =  ii|jj;