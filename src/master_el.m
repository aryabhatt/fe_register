function [ke] = master_el (x, y, lambda, mu)

ke = zeros (6,6);
dNdx = [-1, 1, 0];
dNdy = [-1, 0, 1];
A = 0.5;
jdet = (x(2)-x(1))*(y(3)-y(1)) - (x(3)-x(1))*(y(2)-y(1));
jinv = [y(3)-y(1), y(1)-y(2); x(1)-x(3), x(2)-x(1)] / jdet;
for i = 1:3
    for j = 1:3
        knd = zeros (2);
        knd(1) = (lambda + 2*mu) * dNdx(i)*dNdx(j) + mu * dNdy(i) * dNdy(j);
        knd(2) = (lambda + mu) * dNdy(i)*dNdx(j);
        knd(3) = (lambda + mu) * dNdx(i)*dNdy(j);
        knd(4) = (lambda + 2*mu) * dNdy(i)*dNdy(j) + mu * dNdx(i) * dNdx(j);
        ibeg = 2*i-1;
        jbeg = 2*j-1;
        ke(ibeg:ibeg+1, jbeg:jbeg+1) = jinv * knd * A * jdet;
    end
end

