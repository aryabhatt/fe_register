function [p, t] = remesh (p, t, ibnd, nel0, hdata, opts)

nelm = size (t, 1);

% if mesh size is too large resample points
while (nelm > 2 * nel0)
    pts = reorder (p(ibnd));
    [p, t] = mesh2d (pts(1:2:end),[], hdata, opts);
end
    