function unew = fem (nodes, elems, surf, ibnd, ifix, pres, use_pres)
%unew = fem (nodes, elems, surf, ibnd, ifix, pres, use_pres)
%     Assmebles the Finite-Element System and Solves it
%     nodes : table of nodes
%     elems : connectivity information of nodes
%     surf  : KD-Tree object for force calculations
%     ibnd  : Outer boundary form force calculations
%     ifix  : boolean array for fixed nodes
%     pres  : Pressure
%     use_pres : Flag to switch between pressure and local force

% Number of nodes
npts = size (nodes, 1);
nfix = length (find(ifix));
unew = zeros (npts, 2);

% assemble global matrix
ksys = assemble_linsys (elems, nodes);

% assemble the force vector
fsys = assemble_rhs (nodes, elems, ibnd, surf, pres, use_pres);

% free nodes are nodes that are not fixed
ndfree = true(npts, 1);
ndfree(ifix) = false;

% ndof: degrees of freedom for each free node
ndof = true (2*npts, 1);
ndof(1:2:end) = ndfree;
ndof(2:2:end) = ndfree;

% solve the linear system 
A = ksys(ndof, ndof);
f = fsys(ndof);
d = A \ f;
unew(ndfree,:) = reshape (d, 2, npts-nfix)';