function [A, pts] = rigid (pts_a, pts_b)
%
%  Find coefficients of affine trarnsformation
%  min ( A * pts_b - pts_a )
%
surf_a = createns (pts_a,'NsMethod','kdtree','distance','minkowski');
options = optimset ('Algorithm', 'levenberg-marquardt');
options.MaxIter = 50;

A0 = [0, 0];
%Irow = [0, 0, 1];
a = lsqnonlin (@objfunc, A0, [], [], options);
disp(a);
A = a(2)*[cos(a(1)), sin(a(1)); -sin(a(1)), cos(a(1))];
pts = (A * pts_b')';

    function f = objfunc (x)
        T = x(2)*[cos(x(1)), sin(x(1)); -sin(x(1)), cos(x(1))];
        pts_c = (T * pts_b')';
        surf_c = createns (pts_c,'NsMethod','kdtree','distance','minkowski');
        f1 = point2surf (surf_a, pts_c);
        f2 = point2surf (surf_c, pts_a);
        f = [f1; f2];
    end
end