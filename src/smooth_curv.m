function pts = smooth_curv (x, y, s)
n = length(x);
if length(y) ~= n
    error ('X and Y must be of same length');
end

if isempty(s)
    s = 10;
end
z = smoothn (complex (x, y), s);
pts = [real(z), imag(z)];