function ke = isoparatri (E, nu, x, y)
jdet = (x(2)-x(1))*(y(3)-y(1)) - (x(3)-x(1))*(y(2)-y(1));
jinv = [y(3)-y(1), y(1)-y(2); x(1)-x(3), x(2)-x(1)] / jdet;

gradu = jinv*[-1, 0, 1, 0, 0, 0; -1, 0, 0, 0, 1, 0];
gradv = jinv*[0, -1, 0, 1, 0, 0; 0, -1, 0, 0, 0, 1];
D = [1, nu, 0; nu, 1, 0; 0, 0, (1-nu)/2];
A = 0.5;
B  = zeros (3, 6);
B(1,:) = gradu(1,:);
B(2,:) = gradv(2,:);
B(3,:) = gradu(2,:) + gradv(1,:);
ke = (E/(1-nu^2)) * B' * D * B * A * jdet;