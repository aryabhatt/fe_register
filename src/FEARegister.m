function M = FEARegister

% create model
%[xfix, yfix] = ellipse ([0,0], 8, 5, 500);

%%%%%% Prepare Fixed Point Cloud %%%%%%
% Read data file
data = csvread('../Bladder04.csv');

% Order the points by distnce
pts = reorder (data(:,1:2), data(2,1:2));

% smooth the data
pts = smooth_curv (pts(:,1), pts(:,2), 20);

% Save fixed points for plotting
xfix = pts (:,1);
yfix = pts (:,2);

% Center of the fixed points
cen = mean (data(:, 1:2));

% Create a K-D Tree object from the fixed points
surf = createns (data(:,1:2), 'NsMethod', 'kdtree', 'distance', 'euclidean');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%% Prepare Moiving Point Cloud %%%%
%[xmov, ymov] = circle ([0,0], 5, 51);
% Read data file
data = csvread('../Bladder06.csv');

% Order the points by distnce
pts = reorder(data(:,1:2), data(2,1:2));

% Align centers
del = cen - mean(pts);
pts = bsxfun(@plus, pts, del);

% smooth the data. out is outer bounary of the shell
out = smooth_curv (pts(:,1), pts(:,2), 20);

% save initial configuration for plotting
xmov = out(:,1);
ymov = out(:,2);

% Artificially create an inner wall
inn = create_shell (pts);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%% Prepare for mesh-generation %%%%
% concatenate outer and inner surface points
pts = [out; inn];
Nout = size(out, 1);
Ninn = size(inn, 1);

% create edge information
i1 = 1:Nout;
j1 = 2:Nout+1;
j1(Nout)=1;
i2 = Nout + (1:Ninn);
j2 = Nout + (2:Ninn+1);
j2(Ninn) = i2(1);
edges = [[i1, i2]', [j1, j2]'];

% mesh-generation options
hmax = 2.5;
hdata = struct('hmax', hmax);
opts = struct('output', false);
[nodes, elems] = mesh2d (pts, edges, hdata, opts);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% this is result of manual calculations 
T = [1.0953 -0.1012 -2.5; 0.1012 1.0953 0; 0 0 1];
tmp = (T * [nodes, ones(size(nodes,1),1)]')';
nodes = tmp(:,1:2);
nelms0 = size (elems, 1);
%%% end manual calculations %%%%%%%%%%%

%%%%% Prepare Control Points for DVF %%
xmin = min(min(xmov), min(xfix));
xmax = max(max(xmov), max(xfix));
ymin = min(min(ymov), min(yfix));
ymax = max(max(ymov), max(yfix));
ax = [xmin, xmax, ymin, ymax];

% dvf
nxctrl = 51;
dx = (xmax - xmin) / (nxctrl - 1);
x = xmin + (0:nxctrl-1) * dx;
nyctrl = floor ((ymax - ymin) / dx) + 1;
y = ymin + (0:nyctrl-1) * dx;
[xctrl, yctrl] = meshgrid (x, y);
uvec = zeros (size(xctrl));
vvec = zeros (size(yctrl));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%% Search Boundary Nodes %%%%%%%%%%
[con, ~, ~, bnd] = connectivity (nodes, elems);
ifix = findfixednodes (nodes, bnd);
[obnd, ibnd] = search_innout (con, bnd);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Initial calculations are done using constant 
%%% pressure. After few iterations, we switch to
%%% forces, based on local discrepencies.
[ds, ~, ~] = point2surf (surf, nodes(obnd,:));
pres = norm (ds, 2);
use_pres = true;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%% Setup iterations %%%%%%%%%%%%%%%
maxIter = 500;
nframes = 25;
n = maxIter / nframes;
k = 1;

%%%%%% Iterate until stopping criteria is met
for i = 1 : maxIter
    
    % Finite-Elements Step
    u = fem (nodes, elems, surf, obnd, ifix, pres, use_pres);
    
    %%% Run stats %%%
    % Switch to force based on local discrepencies
    % once the maximum displacement falls below a
    % certain predefined threshold
    umax = norm (u, inf);
    if i == 1
        umax_0 = umax;
    elseif (umax/umax_0 < 0.05)
        use_pres = false;
    end
    
    % Stopping criteria
    [s, ~, ~] = point2surf (surf, nodes(obnd,:));
    err = norm (s, 2);
    if (err < 0.01)
        break;
    end

    %%%%%% Update DVF %%%%%%%
    [du, dv]  = interpoldvf (nodes, elems, u, xctrl, yctrl);
    uvec = uvec + du;
    vvec = vvec + dv;
    
    %%%%%% Deform the Mesh %%%%%
    nodes = nodes + u;
    
    %%%%%% Output %%%%%%%%%
    if mod (i, n) == 0
        plot (xfix, yfix, 'b.');
        patch('Faces', elems, 'Vertices', nodes, 'Facecolor', 'w');
        axis (ax);
        drawnow;
        M(k) = getframe;
        k = k+1;
        fprintf ('at iter %d, umax = %f, err = %f\n', i, umax, err);
        if (~use_pres)
            keyboard;
        end
    end

    % check mesh quality and refine wherever required
    qual = quality (nodes, elems);
    qavg = mean (qual);
    %qstd = std (qual);
    if (qavg < 0.75)
        [nodes, elems] = remesh (nodes, elems, ibnd, nelms0, hdata, opts);
%     elseif (length (qual(qual < 0.7)) < 0.02 * size (elems,1))
%         elems2refine = false (size (elems, 1), 1);
%         elems2refine (qual < 0.7) = true;
%         [nodes, elems] = refine (nodes, elems, elems2refine);
    end
    [~, ~, ~, ibnd] = connectivity (nodes, elems);
    %fprintf ('After %d iterations:\n', i);
    %fprintf ('Mean mesh quality: %6.4f, stdev = %6.4f\n', qavg, qstd);
end
plot (xfix, yfix, 'r.');
patch('Faces', elems, 'Vertices', nodes, 'Facecolor', 'w');
figure;
hold on;
plot (xfix, yfix, 'k.', xmov, ymov, 'r.');
quiver (xctrl, yctrl, uvec, vvec);
hold off;
