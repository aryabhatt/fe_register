function [d, u, v] = point2surf (surf, pts)

% sanity check
if (~isa (surf, 'KDTreeSearcher'))
    error ('Type Mismatch: first argument should of of type : "KDTreeSearcher"');
end

% allocate memory
na = size(pts, 1);
d = zeros (na, 1);
u = zeros (na, 1);
v = zeros (na, 1);

% distance between points and surface
for i = 1 : na
    [j, s] = knnsearch (surf, pts(i,:), 'k', 1);
    
    % find the distance and its componets
    %[ds,dx,dy] = mindist (surf.X(j,:), pts(i,:));
    
    d(i) = s;
    u(i) = surf.X(j,1) - pts(i,1);
    v(i) = surf.X(j,2) - pts(i,2);
end