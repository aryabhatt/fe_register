function [u, v] = dist2ellipse (major, minor, pt)
npts = size (pt,1);
u = zeros (npts, 1);
v = zeros (npts, 1);
for i = 1 : npts
    [~, x, y] = dist2ell (major, minor, pt(i,:));
    u(i) = x - pt(i,1);
    v(i) = y - pt(i,2);
end
