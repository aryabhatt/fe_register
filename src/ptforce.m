function [s, u, v] = ptforce (surf, pt, nrm)
ibeg = 1;
iend = 16;
nmax = size(surf.X, 1);
[jj, ds] = knnsearch (surf, pt, 'k', iend);

% if point is already near boundary, not search
j = jj(1);
dx = surf.X(j,1) - pt(1);
dy = surf.X(j,2) - pt(2);
if (abs(dot([dx, dy], nrm)) < 0.75) 
    u = 0;
    v = 0;
    s = eps;
else
    u = dx;
    v = dy;
    s = ds(1);
end

% % otherwise search
% while true 
%     for i = ibeg : iend
%         j = jj(i);
%         eij =[surf.X(j,1) - pt(1), surf.X(j,2) - pt(2)] / ds(i);
%         cost = dot (eij, nrm);
%         if (abs(cost) > 0.9397)
%             if ( ds(i) > 0.25 )
%                 u = surf.X (j,1) - pt(1);
%                 v = surf.X (j,2) - pt(2);
%             else
%                 u = 0;
%                 v = 0;
%             end
%             return;
%         end
%     end
%     ibeg = iend + 1;
%     iend = min(iend * 2, nmax);
%     [jj, ds] = knnsearch (surf, pt, 'k', iend);
%     if (ibeg > nmax)
%         error ('Did not find what I was looking for.');
%     end
% end